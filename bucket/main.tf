module "config" {
  source = "../config"
}

module "bucket" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/minecraft//bucket?ref=v2"

  region      = module.config.region
  lifespan    = module.config.lifespan
  application = module.config.application
  environment = module.config.environment
  role        = module.config.role
  tags        = module.config.tags
}
