provider "aws" {
  region  = "us-east-1"
  version = "~> 2.42"
}

terraform {
  required_version = "0.12.24"

  backend "s3" {
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt        = "true"

    bucket = "bixbots-terraform-state"
    key    = "lvl1/minecraft/bucket"
  }
}
