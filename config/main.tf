output "region" {
  value = "us-east-1"
}

output "lifespan" {
  value = "permanent"
}

output "application" {
  value = "minecraft"
}

output "environment" {
  value = "lvl1"
}

output "role" {
  value = "server"
}

output "tags" {
  value = {}
}

#

output "timezone" {
  value = "America/New_York"
}

#

output "users" {
  value = []
}

output "operators" {
  value = [
    "Spooners21", # Marcin
    "ScarHolmes", # Steffanie
  ]
}

output "gamemode" {
  value = "survival"
}

output "difficulty" {
  value = "normal"
}

output "motd" {
  value = "Minecraft"
}

output "java_mem" {
  value = "4096M"
}

output "max_players" {
  value = 20
}

resource "random_password" "rcon_pwd" {
  length = 16

  min_upper   = 2
  min_lower   = 2
  min_numeric = 2
  min_special = 2

  special          = true
  override_special = "!@#%-_+"
}

output "rcon_pwd" {
  value = random_password.rcon_pwd.result
}

#

output "subnet_type" {
  value = "public"
}

output "instance_type" {
  value = "c5d.xlarge"
}

output "spot_price" {
  value = "0.192"
}

output "root_block_device" {
  value = {
    enabled     = true
    volume_type = "gp2"
    volume_size = 40
  }
}

output "cert_subject" {
  value = "lvl1.cloud"
}
