# lvl1/minecraft

## Summary

Deploys a minecraft server to the `lvl1` environment.

* Address: `minecraft.lvl1.cloud`
* Map: https://minecraft.lvl1.cloud/map

## Details

See the following terraform modules for more information:
* [minecraft/bucket](https://gitlab.com/bixbots/cloud/terraform-modules/minecraft/-/tree/master/bucket)
* [minecraft/server](https://gitlab.com/bixbots/cloud/terraform-modules/minecraft/-/tree/master/server)

### Configuration ###

* open to public (i.e. whitelist disabled)
* gamemode: survival
* difficulty: normal
* max-players: 20

### Plugins ###

* [Dynmap](https://dev.bukkit.org/projects/dynmap)
* [EssentialsX](https://www.spigotmc.org/resources/essentialsx.9089/)
* [LuckPerms](https://luckperms.net/)
* [Vault](https://www.spigotmc.org/resources/vault.34315/)
* [BiomeFinder](https://www.spigotmc.org/resources/biomefinder.30892/)
* [ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/)
* [xRay](https://www.spigotmc.org/resources/xray.42228/)

### Operators ###

* Spooners21
* ScarHolmes

### Default Permissions ###

* essentials.afk
* essentials.afk.auto
* essentials.back
* essentials.balance
* essentials.balancetop
* essentials.compass
* essentials.delwarp
* essentials.getpos
* essentials.help
* essentials.helpop
* essentials.home
* essentials.kittycannon
* essentials.list
* essentials.mail
* essentials.msg
* essentials.near
* essentials.pay
* essentials.r
* essentials.sethome
* essentials.setwarp
* essentials.tp
* essentials.tpa
* essentials.tpaall
* essentials.tpacancel
* essentials.tpaccept
* essentials.tpahere
* essentials.tpauto
* essentials.tpdeny
* essentials.tppos
* essentials.warp
* biomefinder.*
* xray.toggle
