variable "bucket_id" {
  type = string
}

module "config" {
  source = "../config"
}

module "ssh_keypair" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/ssh-keypair.git?ref=v1"

  application = module.config.application
  environment = module.config.environment

  public_key = file("${path.module}/minecraft-lvl1.pubkey.pem")
}

module "server" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/minecraft//server?ref=v3"

  region      = module.config.region
  lifespan    = module.config.lifespan
  application = module.config.application
  environment = module.config.environment
  role        = module.config.role
  tags        = module.config.tags

  bucket_id = var.bucket_id

  subnet_type       = module.config.subnet_type
  instance_type     = module.config.instance_type
  spot_price        = module.config.spot_price
  key_name          = module.ssh_keypair.key_name
  root_block_device = module.config.root_block_device
  cert_subject      = module.config.cert_subject
  timezone          = module.config.timezone

  users       = module.config.users
  operators   = module.config.operators
  gamemode    = module.config.gamemode
  difficulty  = module.config.difficulty
  motd        = module.config.motd
  java_mem    = module.config.java_mem
  max_players = module.config.max_players
  rcon_pwd    = module.config.rcon_pwd
}
